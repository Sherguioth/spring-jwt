package io.github.sherguioth.jwt.services;

import io.github.sherguioth.jwt.domain.Role;
import io.github.sherguioth.jwt.domain.User;

import java.util.List;

public interface UserService {
    User saveUser(User user);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    User getUser(String username);
    List<User> getUsers();
}
