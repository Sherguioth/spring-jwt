package io.github.sherguioth.jwt;

import io.github.sherguioth.jwt.domain.Role;
import io.github.sherguioth.jwt.domain.User;
import io.github.sherguioth.jwt.services.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class SpringJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJwtApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    CommandLineRunner run(UserService userService) {
        return args -> {
            userService.saveRole(new Role(null, "ROLE_USER"));
            userService.saveRole(new Role(null, "ROLE_MANAGER"));
            userService.saveRole(new Role(null, "ROLE_ADMIN"));
            userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

            userService.saveUser(new User(null, "Dwayne Johnson", "the-rock", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Will Smith", "will", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Jim Carry", "jim", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Keanu Reeves", "john-wick", "1234", new ArrayList<>()));

            userService.addRoleToUser("the-rock", "ROLE_USER");
            userService.addRoleToUser("the-rock", "ROLE_ADMIN");
            userService.addRoleToUser("will", "ROLE_USER");
            userService.addRoleToUser("will", "ROLE_MANAGER");
            userService.addRoleToUser("jim", "ROLE_USER");
            userService.addRoleToUser("john-wick", "ROLE_SUPER_ADMIN");
            userService.addRoleToUser("john-wick", "ROLE_ADMIN");
            userService.addRoleToUser("john-wick", "ROLE_USER");
        };
    }
}
