package io.github.sherguioth.jwt.repo;

import io.github.sherguioth.jwt.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long>{
    Role findByName(String name);
}
